# README #
 Coding challenge pertaining to create an application to present most used words from a feed in a wordcloud web page.

## Quick summary ##
* Do connect to Twitter with Twitter API and fetch tweets that match a search criteria
- Save the tweets in a local in-memory database H2
- Process the saved tweets to be classified by hashtag frequency
- Use Rest services to show the classification in a word cloud and to view tweets related to the selected hashtag.

### Prerequisites###
- Java 8
- Maven 3+

### Guide ###
- Clone project or download source code locally
- run maven command 

```
#!maven
mvn clean package spring-boot:run
```
Open your browser at 

```
#!HTTP

http://localhost:8888/wordcloud
```


### Showcase ###
- Java 8 features
- Spring boot
- Spring Data JPA, Spring Data Rest
- Rest api
- Web application, angularjs, bootstrap
- Twitter api
- In-memory database