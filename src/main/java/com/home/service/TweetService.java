package com.home.service;

import com.home.model.SavedTweet;
import com.home.repository.TweetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.twitter.api.SearchResults;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by salae on 2016-10-08.
 */
@Service
public class TweetService {


    private ConnectionRepository connectionRepository;
    private Twitter twitter;
    private String searchString;
    private TweetRepository tweetRepository;

    @Autowired
    public TweetService(Twitter twitter,
                        ConnectionRepository connectionRepository,
                        @Value("${twitter.search.query}") String searchString,
                        TweetRepository repository) {
        this.connectionRepository = connectionRepository;
        this.twitter=twitter;
        this.searchString=searchString;
        this.tweetRepository=repository;

    }

    @Scheduled(fixedDelay = 10000, initialDelay = 5000)
    public void feedTweets(){
        SearchResults results = twitter.searchOperations().search(searchString, 400);
        results.getTweets()
                .stream()
                .forEach(tweet -> {
                    tweetRepository.save(createTweet.apply(tweet));
                });
    }

    private Function<Tweet, SavedTweet> createTweet = (Tweet t) ->
                new SavedTweet(t.getId(),
                        t.getCreatedAt(),
                        t.getRetweetCount(),
                        t.getText(),
                        getHashtags(t));

    private List<String> getHashtags(Tweet t){
        return t.getEntities().getHashTags()
                .stream()
                .map(hashtag -> hashtag.getText())
                .collect(Collectors.toList());
    }

}
