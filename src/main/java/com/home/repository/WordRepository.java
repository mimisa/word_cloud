package com.home.repository;

import com.home.model.Word;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by salae on 2016-10-08.
 */
public interface WordRepository extends CrudRepository<Word,Long>{


}
