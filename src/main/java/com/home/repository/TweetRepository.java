package com.home.repository;

import com.home.model.SavedTweet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;
import java.util.Set;

/**
 * Created by salae on 2016-10-08.
 */
public interface TweetRepository  extends CrudRepository<SavedTweet,Long> {

    @RestResource(path = "/bytag")
    Set<SavedTweet> findByTextContaining(@Param("tag") String tag);
}
