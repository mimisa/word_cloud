package com.home.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by salae on 2016-10-08.
 */

@Entity
@Table(name ="WORD_FREQUENCY" )
public class Word{

    @Id
    @Column(name = "ID")
    private String text;

    private Long weight;

    public Word(String text, Long weight) {
        this.text = text;
        this.weight = weight;
    }

    public Word(){}

    public String getText() {
        return text;
    }

    public Long getWeight() {
        return weight;
    }
}
